# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv
import openerp.addons.decimal_precision as dp

class sprintit_product_template(osv.osv):
    _inherit = 'product.template'
    _description = 'Products'
    _columns = {
        'x_recycle_type': fields.selection([('refill','Refill'), 
                                            ('recycle','Recycle'), 
                                            ('disposable','Disposable'), 
                                            ('no_package','No Package')], 'Recycling Type'),
        'x_recycling_type': fields.selection([('bottle','Bottle'), 
                                              ('can','Can'), 
                                              ('carton','Carton'), 
                                              ('keg','Keg'), 
                                              ('canister','Canister'), 
                                              ('plastic_bottle','Plastic Bottle'), 
                                              ('barrel','Barrel'), 
                                              ('bulk','Bulk'), 
                                              ('bib','BiB'), 
                                              ('other','Other')], 'Package Type'),
        'x_packing_type': fields.selection([('d_domestic_kegging','Domestic (Domestic Kegging)'), 
                                            ('d_foreign_kegging','Domestic (Foreign Kegging)'), 
                                            ('f_domestic_kegging','Foreign (Domestic Kegging)'), 
                                            ('f_foreign_kegging','Foreign (Foreign Kegging'),
                                            ('domestic_licenced','Domestic Lincenced Product')], 'Place of Packaging'),
        'x_package_size': fields.float('Packages Size', help="Package size in Litres", digits_compute= dp.get_precision('Product UoS')),
        'x_alcohol_percentage': fields.float('Alcohol vol-%', help="Alcohol Volume Percentage", digits_compute= dp.get_precision('Product UoS')),
        'x_gravity': fields.float('Gravity %', help="Gravity in Weight Percentage", digits_compute= dp.get_precision('Product UoS')),
        
        'x_package_type_valvira': fields.integer('Package Id(Valvira)', help="Valvira Package Type"),
        'x_fg': fields.boolean('Finished Goods(FG)', help="Finished Goods Product"),
        'x_sfg': fields.boolean('Semi Finished Goods(SFG)', help="Semi Finished Goods Product"),
        
        }
    
sprintit_product_template()


class product_packaging(osv.osv):
    _inherit = 'product.packaging'
    _columns = {
        'x_dun': fields.char('DUN code', size=32, help="DUN-14 (Distribution Unit Number)"),
        }
    
    
product_packaging()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: