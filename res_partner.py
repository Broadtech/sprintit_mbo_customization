# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv

class sprintit_res_partner(osv.osv):
    _inherit = 'res.partner'
    _description = 'Partner'
    _columns = {
        'x_customer_type': fields.selection([('export','Export'), 
                                             ('other','Other'), 
                                             ('restaurant','Restaurant'), 
                                             ('retail','Retail'), 
                                             ('supplier','Supplier'), 
                                             ('temporary_occasion','Temporary Occasion'), 
                                             ('warehouse_manufacturer','Warehouse, Manufacturer(duty free)'), 
                                             ('wholesale_duty_free','Wholesale(duty free)'), 
                                             ('wholesale','wholesale(duty)')], 'Type'),
        'x_licence_id': fields.char('Licence Number', size=32),
        'x_business_id': fields.char('Business id', size=32),
        'x_customs_wh': fields.char('Customs Warehouse', size=16),
        'x_palpa': fields.selection([('yes','Yes'), ('no','No')], 'Palpa'),
        'x_customer_licence': fields.selection([('valid','Valid'), ('not_valid','Not Valid')], 'Licence validity',
                                                help="Customer licence validity as per Valvira registry."),
        'x_valvira_licence_class': fields.selection([('TUVA','TUVA'), ('AB','AB'), ('C','C')], 'License class',
                                                help="Valvira license class."),
        
        }
    
sprintit_res_partner()



class res_country(osv.osv):
    _inherit = 'res.country'
    _columns = {
        'x_code': fields.char('3-digit Country Code', size=3),
        }
    
res_country()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: