# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _

class sale_order(osv.osv): 
    _inherit = 'sale.order'
    _description = 'Sale Order'

    def onchange_partner_id(self, cr, uid, ids, part, order_line, context=None):
        """Update Route in Sale Order Line corresponding to partner_id in Sale Order"""
        if context is None:
            context = {}
        orderlines = []
        parameter_pool = self.pool.get('sale.parameter.table')
        result = super(sale_order, self).onchange_partner_id(cr, uid, ids, part, context=context)
        if part and order_line:
            partner_obj = self.pool.get('res.partner').browse(cr, uid, part, context=context)
            cus_type = partner_obj.x_customer_type
            parameter_id = parameter_pool.search(cr, uid, [('customer_type', '=', cus_type)], context=context)
            default_route_id = False
            if parameter_id:
                parameter_obj = parameter_pool.browse(cr, uid, parameter_id[0], context=context)
                default_route_id = parameter_obj.default_route_id.id
                
            for line in order_line:
                if line[0] in [0, 1]:
                    line[2]['route_id'] = default_route_id
                    orderlines.append(line)
                elif line[0] in [4, 6]:
                    line_ids = line[0] == 4 and [line[1]] or line[2]
                    for line_id in line_ids:
                        orderlines.append([1, line_id, {'route_id': default_route_id}])
                else:
                    orderlines.append(line)
                    
            result['value'].update({'order_line': orderlines})
        return result
    
    def action_wait(self, cr, uid, ids, context=None):
        parameter = self.pool.get('customer.licence.parameter.table')
        for sale in self.browse(cr, uid, ids):
            if sale.partner_id and sale.partner_id.x_customer_licence != 'valid':
                raise osv.except_osv(_('Warning!'),_('Customer licence is not valid.'))
            
            parameter_ids = parameter.search(cr, uid, [('licence_type', '=', 'C')], context=context)
            if parameter_ids and sale.partner_id and sale.partner_id.x_valvira_licence_class == 'C':
                parameter_obj = parameter.browse(cr, uid, parameter_ids[0], context=context)
                for line in sale.order_line:
                    if line.product_id and line.product_id.x_alcohol_percentage > parameter_obj.x_valvira_c_licence:
                        raise osv.except_osv(_('Warning!'),_('Alcohol percentage exceeds allowed limit.'))
                        
        res = super(sale_order, self).action_wait(cr, uid, ids, context=context)
        return res
    
sale_order()

class sale_order_line(osv.osv): 
    _inherit = 'sale.order.line'
    _description = 'Sale Order Line'
     
    def default_get(self, cr, uid, fields, context=None):
        """Load Route in Sale Order Line corresponding to partner_id in Sale Order"""
        res = super(sale_order_line, self).default_get(cr, uid, fields, context=context)
        parameter_pool = self.pool.get('sale.parameter.table')
        partner_id = 'default_partner_id' in context and context['default_partner_id'] or False
        if partner_id:
            partner_obj = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            cus_type = partner_obj.x_customer_type
            parameter_id = parameter_pool.search(cr, uid, [('customer_type', '=', cus_type)], context=context)
            if parameter_id:
                parameter_obj = parameter_pool.browse(cr, uid, parameter_id[0], context=context)
                res.update({'route_id': parameter_obj.default_route_id.id})
        return res
      
sale_order_line()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
