# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################


import product
import res_partner
import parameter_table
import sale
import mrp
import stock

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: