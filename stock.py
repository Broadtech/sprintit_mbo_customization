# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv

class stock_inventory(osv.osv): 
    _inherit = 'stock.inventory'
    _description = 'Stock Inventory'
    _columns = {
        'picking_type_id': fields.many2one('stock.picking.type', 'Stock Picking Type', readonly=True, states={'draft': [('readonly', False)]}),
        }
    
    
    def action_done(self, cr, uid, ids, context=None):
        """Update Stock Picking type in created stock """
        res = super(stock_inventory, self).action_done(cr, uid, ids, context=context)
        for inv_obj in self.browse(cr, uid, ids, context=context):
            move_ids = [mov_obj.id for mov_obj in inv_obj.move_ids]
            if move_ids and inv_obj.picking_type_id:
                self.pool.get('stock.move').write(cr, uid, move_ids, {'picking_type_id': inv_obj.picking_type_id.id}, context=context)
        return res
    
    
stock_inventory()


class stock_picking(osv.osv): 
    _inherit = 'stock.picking'
    _description = 'Stock Picking'


    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        """Update Stock Picking type in Picking corresponding to partner_id in Picking """
        parameter_pool = self.pool.get('sale.parameter.table')
        if partner_id:
            partner_obj = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            parameter_id = parameter_pool.search(cr, uid, [('customer_type', '=', partner_obj.x_customer_type)], context=context)
            if parameter_id:
                parameter_obj = parameter_pool.browse(cr, uid, parameter_id[0], context=context)
                if parameter_obj.default_route_id.pull_ids:
                    return {'value': {'picking_type_id': parameter_obj.default_route_id.pull_ids[0].picking_type_id.id}}
        return {'value': {'picking_type_id': False}}
    
    
stock_picking()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
