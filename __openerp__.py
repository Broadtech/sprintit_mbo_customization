# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################
{
    'name': 'Sprintit MBO Customization',
    'version': '1.8',
    'category': 'Sprintit MBO Customization',
    'description': """Customer and Product Customization""",
    'author': 'SprintIT, Roy Nurmi',
    'maintainer': 'SprintIT, Roy Nurmi',
    'website': 'http://www.sprintit.fi',
    'images': [],
    'depends': ['base_vat', 'sale', 'mrp', 'stock_account', 'sprintit_company_information'],
    'data': [
        'security/ir.model.access.csv',
        'product_view.xml',
        'res_partner_view.xml',
        'parameter_table_view.xml',
        'cron_data.xml',
        'sale_view.xml',
        'mrp_view.xml',
        'stock_view.xml'
        ],
    'demo': [],
    'test': [],
    'installable': True,
    'auto_install': False,
    'application': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: