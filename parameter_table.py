# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv
import base64#file encode
import urllib2 #file download from url

class sprintit_parameter_table(osv.osv):
    _name = 'sale.parameter.table'
    _description = 'Parameter Table'
    _columns = {
        'customer_type': fields.selection([('export','Export'), 
                                           ('other','Other'), 
                                           ('restaurant','Restaurant'), 
                                           ('retail','Retail'), 
                                           ('supplier','Supplier'), 
                                           ('temporary_occasion','Temporary Occasion'), 
                                           ('warehouse_manufacturer','Warehouse, Manufacturer(duty free)'), 
                                           ('wholesale_duty_free','Wholesale(duty free)'), 
                                           ('wholesale','wholesale(duty)')], 'Customer Type'),
        'default_route_id': fields.many2one('stock.location.route', 'Default Route'),
        'active': fields.boolean('Active'),
        }
    _defaults = {
        'active': True,
        }
    
sprintit_parameter_table()


class sprintit_mrp_parameter_table(osv.osv):
    _name = 'mrp.parameter.table'
    _description = 'MRP Parameter Table'
    _columns = {
        'category_id': fields.many2one('product.category', 'Product Category'),
        'default_route_id': fields.many2one('stock.location.route', 'Default Route'),
        'active': fields.boolean('Active'),
        }
    _defaults = {
        'active': True,
        }
    
sprintit_parameter_table()


class sprintit_customer_licence_parameter_table(osv.osv):
    _name = 'customer.licence.parameter.table'
    _description = 'Customer Licence Parameter Table'
    
    _columns = {
        'licence_type': fields.selection([('TUVA','TUVA'), ('AB','AB'), ('C','C')], 'License class',
                                            help="Valvira license class."),
        'url': fields.char('URL', size=128),
        'x_valvira_c_licence': fields.float('Valvira C-license alc-%', digits=(16,3),
                                            help="Max alcohol volume percentage allowed for C-license customer.") 
        }
    
    def _check_validity(self, cr, uid, context=None):
        partner = self.pool.get('res.partner')
        partner_ids = partner.search(cr, uid, [], context=context)
        if partner_ids:
            vals = {
                'x_customer_licence': 'not_valid',
                'x_valvira_licence_class': ''
                }
            partner.write(cr, uid, partner_ids, vals, context=context)
            
        parameter_ids = self.search(cr, uid, [], context=context)
        parameter_objs = self.browse(cr, uid, parameter_ids, context=context)
        for parameter_obj in parameter_objs:
            link = parameter_obj.url
            file = urllib2.urlopen(link).read()
            for line in file.split('\n'):
                licence_no = line[21:29].strip()
                for partner_id in partner_ids:
                    partner_obj = partner.browse(cr, uid, partner_id, context=context)
                    if partner_obj.x_licence_id and partner_obj.x_licence_id == licence_no:
                        vals = {
                            'x_customer_licence': 'valid',
                            'x_valvira_licence_class': parameter_obj.licence_type
                            }
                        partner.write(cr, uid, [partner_id], vals, context=context)
        return True
    
    def action_check_validity(self, cr, uid, ids, context=None):
        self._check_validity(cr, uid, context)
        return True
    
sprintit_customer_licence_parameter_table()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: