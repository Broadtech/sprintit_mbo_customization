# -*- coding: utf-8 -*-
##############################################################################
#
#    ODOO Open Source Management Solution
#
#    ODOO Addon module by Sprintit Ltd
#    Copyright (C) 2015 Sprintit Ltd (<http://sprintit.fi>).
#
##############################################################################

from openerp.osv import fields, osv


class mrp_production(osv.osv): 
    _inherit = 'mrp.production'
    _description = 'Manufacturing Order'
    _columns = {
        'default_route_id': fields.many2one('stock.location.route', 'Default Route', readonly=True, states={'draft': [('readonly', False)]}),
        }
    
    
    def product_id_change(self, cr, uid, ids, product_id, product_qty=0, context=None):
        res = super(mrp_production, self).product_id_change(cr, uid, ids, product_id, product_qty, context=context)
        parameter_pool = self.pool.get('mrp.parameter.table')
        if product_id:
            product_obj = self.pool.get('product.product').browse(cr, uid, product_id, context=context)
            parameter_ids = parameter_pool.search(cr, uid, [('category_id', '=', product_obj.categ_id.id)], context=context)
            if parameter_ids:
               parameter_obj = parameter_pool.browse(cr, uid, parameter_ids[0], context=context)
               res['value'].update({'default_route_id': parameter_obj.default_route_id.id})
        return res
        
    
    def action_produce(self, cr, uid, production_id, production_qty, production_mode, wiz=False, context=None):
        res = super(mrp_production, self).action_produce(cr, uid, production_id, production_qty, production_mode, wiz, context=context)
        """
        Update Stock picking type in the stock move related to the produced products
        """
        production = self.browse(cr, uid, production_id, context=context)
        if production.default_route_id:
            picking_type_id = False
            if production.default_route_id.pull_ids:
                picking_type_id = production.default_route_id.pull_ids[0].picking_type_id.id
            move_ids = [move.id for move in production.move_created_ids2]
            if picking_type_id and move_ids:
                self.pool.get('stock.move').write(cr, uid, move_ids, {'picking_type_id': picking_type_id}, context=context)
        return res
    
mrp_production()
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
